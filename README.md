# How to publish new versions of a docker image
* `docker build <location_of_dockerfile>` (e.g. `docker build .`)
* `docker tag <hash> <your_hub_username>/<image_name>:<tag>` (e.g. `docker tag bb38976d03cf bluefu/mariadb-de:10`)
* `docker login --username=yourhubusername --email=youremail@company.com`
* `docker push <your_hub_username>/<image_name>:<tag>` (e.g. `docker push bluefu/mariadb-de:10`)


# Where can i find a better tutorial?
[Here](https://ropenscilabs.github.io/r-docker-tutorial/04-Dockerhub.html)

